package test;

public class Student {

    final String classid="15";

    private static String school;

    String id;
    String name;
    private int age;

    public Student(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public static String getSchool() {
        return school;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static void main(String[] args) {

        Student wu = new Student("1", "wu");
        wu.setAge(20);
        System.out.println("id: "+wu.id+"\nname: "+wu.name+"\n年龄: "+wu.getAge()+"\nclassid: "+wu.classid);
        Student.school="山东电子职业";
        System.out.println("Student类的学校: "+Student.getSchool());

    }

}
