package test

import org.apache.flink.streaming.api.scala._


object FlinkWordCountCreateLocal {
  def main(args: Array[String]): Unit = {
    //1.创建流计算执⾏行行环境
    val env = StreamExecutionEnvironment.createLocalEnvironment(3)
    //2.创建DataStream - 细化
    val text = env.socketTextStream("master", 9999)
    //3.执⾏行行DataStream的转换算⼦子
    val counts = text.flatMap(line => line.split("\\s+"))
      .map(word => (word, 1))
      .keyBy(0)
      .sum(1)
    //4.将计算的结果在控制打印
    counts.print()
    //5.执⾏行行流计算任务
    env.execute("Window Stream WordCount")
  }
}

