import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JedioDemo1 {

    public static void main(String[] args) {
        // 创建一个Jedis对象
        Jedis jedis = new Jedis("master",6379);

        jedis.auth("1234");

        // 测试
        String ping = jedis.ping();
        System.out.println(ping);
    }

    // 操作key String类型
    @Test
    public void StrRd(){
        // 创建一个Jedis对象
        Jedis jedis = new Jedis("master",6379);
        jedis.auth("1234");

        // 添加
        jedis.mset("name1","lucy1","name2","peter","name3","john");
        List<String> mget = jedis.mget("name1", "name2", "name3");
        System.out.println(mget);

        // 获取
        String name = jedis.get("name");
        System.out.println(name);
        // 打印所有建
        Set<String> keys = jedis.keys("*");
        for (String key : keys) {
            System.out.println(key);
        }
    }

    //
    @Test
    public void ListRd(){
        // 创建一个Jedis对象
        Jedis jedis = new Jedis("master",6379);
        jedis.auth("1234");

        jedis.lpush("key1","lucy","mary","sara");
        List<String> key1 = jedis.lrange("key1", 0, -1);
        System.out.println(key1);

    }

    @Test
    public void SetRd(){
        // 创建一个Jedis对象
        Jedis jedis = new Jedis("master",6379);
        jedis.auth("1234");

        jedis.sadd("names","lucy","lucy","jack");
        Set<String> name = jedis.smembers("names");
        System.out.println(name);
    }

    @Test
    public void HashRd(){
        // 创建一个Jedis对象
        Jedis jedis = new Jedis("master",6379);
        jedis.auth("1234");

        // 一条一条添加
        jedis.hset("users","age","20");
        String hget = jedis.hget("users", "age");
        System.out.println(hget);
        // 添加整个map集合
        Map<String, String> map = new HashMap<String,String>();
        map.put("telephone","19412133358");
        map.put("address","atguigu");
        map.put("email","abc@163.com");
        jedis.hmset("hash2",map);
        // 取值
        Set<String> hash2 = jedis.hkeys("hash2");
        List<String> hash2V = jedis.hvals("hash2");
        System.out.println(hash2);
        System.out.println(hash2V);

    }

    @Test
    public void ZsetRd(){
        // 创建一个Jedis对象
        Jedis jedis = new Jedis("master",6379);
        jedis.auth("1234");

        jedis.zadd("china",100d,"shanghai");

        Set<String> china = jedis.zrange("china", 0, -1);
        System.out.println(china);

    }

}
