import redis.clients.jedis.Jedis;

import java.util.Random;

public class PhoneCode {

    public static void main(String[] args) {

//        verifyCode("123456789");
        getRedisCode("123456789","812014");


    }

    // 1.生成6位数字验证码
    public static String getCode(){
        Random random = new Random();
        String code ="";
        for (int i=0;i<6;i++){
            int i1 = random.nextInt(10);
            code+=i1;
        }
        return code;
    }

    // 2.每个手机每天只能发送三次，验证码放到redis中， 设置过期时间
    public static void verifyCode(String phone){
        // 连接redis
        Jedis jedis = new Jedis("master",6379);
        jedis.auth("1234");

        // 手机发送次数key
        String countKey = "VerifyCode"+phone+":count";
        // 验证码key
        String codeKey = "VerifyCode"+phone+":code";

        // 每个手机每天只能发送三次
        String Count = jedis.get(countKey);
        if (Count == null){
            // 没有发送次数，第一次发送
            // 设置发送次数是1
            jedis.setex(countKey,24*60*60,"1");
        }else if(Integer.parseInt(Count)<=2){
            // 发送次数+1
            jedis.incr(countKey);
        }else {
            // 发送三次，不能再发送
            System.out.println("今天发送次数已经超过三次");
            jedis.close();
            return;
        }

        // 发送代码 设置过期时间
        String code1 = getCode();
        jedis.setex(codeKey,120,code1);

        jedis.close();
    }

    // 3.验证码校验
    public static void getRedisCode(String phone,String code){
        Jedis jedis = new Jedis("master",6379);
        jedis.auth("1234");

        String yanzhengCode = "VerifyCode"+phone+":code";
        String s = jedis.get(yanzhengCode);

        if (code.equals(s)){
            System.out.println("成功");
        }else {
            System.out.println("失败");
        }

    }

}
